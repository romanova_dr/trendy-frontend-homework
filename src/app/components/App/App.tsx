import React, { FC } from 'react';
import { MainPage } from '@components/MainPage/MainPage';

export const App: FC = () => {
  return <MainPage />;
};
