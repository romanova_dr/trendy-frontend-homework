import axios from 'axios';
import { IRepo } from '@features/repo/types';

export const fetchRepos = (): Promise<IRepo[]> => {
  return axios
    .get("https://api.github.com/search/repositories?q=javascript&sort=stars&order=desc'")
    .then((res) => res?.data?.items ?? []);
};
