import { createSlice, PayloadAction } from '@reduxjs/toolkit';
import { IRepo } from './types';

interface InitialState {
  repos: IRepo[];
}

const initialState: InitialState = {
  repos: [],
};

export const reposListSlice = createSlice({
  name: 'reposList',
  initialState,
  reducers: {
    setRepos: (state, action: PayloadAction<IRepo[]>) => {
      state.repos = action.payload;
    },
  },
});

export const { setRepos } = reposListSlice.actions;

export default reposListSlice.reducer;
