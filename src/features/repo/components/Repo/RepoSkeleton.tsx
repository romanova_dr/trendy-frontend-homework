import React, { FC } from 'react';
import classNames from 'classnames';
import { ArrowIcon } from '@components/Icons/ArrowIcon/ArrowIcon';
import { StarIcon } from '@components/Icons/StarIcon/StarIcon';
import { ForkIcon } from '@components/Icons/ForkIcon/ForkIcon';
import { SkeletonText } from '@components/SkeletonText/SkeletonText';
import './Repo.css';

export const RepoSkeleton: FC = () => {
  return (
    <div className={classNames('repo')}>
      <ArrowIcon className="repo__arrow-button" position="left" />

      <div className="info__container">
        <div className="repo__info">
          <span className="repo__name">
            <SkeletonText primary />
          </span>
          <span className="repo__description">
            <SkeletonText rowsCount={3} />
          </span>
          <div className="repo__counts">
            <div className="counts__item counts__item_skeleton">
              <StarIcon />
              <span className="counts__title">
                <SkeletonText />
              </span>
            </div>
            <div className="counts__item counts__item_skeleton">
              <ForkIcon />
              <span className="counts__title">
                <SkeletonText />
              </span>
            </div>
          </div>
        </div>
      </div>
      <ArrowIcon className="repo__arrow-button" position="right" />
    </div>
  );
};
