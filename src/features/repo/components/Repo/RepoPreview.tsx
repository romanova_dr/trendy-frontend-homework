import React, { FC } from 'react';
import { IRepo } from '@features/repo/types';
import { StarIcon } from '@components/Icons/StarIcon/StarIcon';
import { ForkIcon } from '@components/Icons/ForkIcon/ForkIcon';
import { LinkIcon } from '@components/Icons/LinkIcon/LinkIcon';
import './Repo.css';

type TProps = {
  repo: IRepo;
};

export const RepoPreview: FC<TProps> = ({ repo }) => {
  const getURL = (url: string) => {
    if (url?.includes('https://')) return url?.slice(8);
    if (url?.includes('http://')) return url?.slice(7);
    return url;
  };

  const getLink = () => {
    if (!repo.homepage) return repo.html_url;
    return repo.homepage;
  };

  return (
    <div className="repo_preview">
      {repo.owner.login && <p className="repo__company">{repo.owner.login}</p>}
      <p className="repo__name repo__name_preview">{repo.name}</p>
      <p className="repo__description">{repo.description}</p>
      <a href={getLink()} target="_blank" rel="noreferrer" className="repo__link">
        <LinkIcon />
        <p className="link__item">{getURL(getLink())}</p>
      </a>
      <div className="repo__tags">
        {repo.topics.map((elem) => (
          <p key={elem} className="tags__item">
            {elem}
          </p>
        ))}
      </div>
      <div className="repo__counts">
        <div className="counts__item">
          <StarIcon />
          <p className="counts__title">{repo.stargazers_count}</p>
        </div>
        <div className="counts__item">
          <ForkIcon />
          <p className="counts__title">{repo.forks_count}</p>
        </div>
      </div>
    </div>
  );
};
