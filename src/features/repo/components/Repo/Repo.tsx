import React, { FC, useState } from 'react';
import classNames from 'classnames';
import { IRepo } from '@features/repo/types';
import { ArrowIcon } from '@components/Icons/ArrowIcon/ArrowIcon';
import { StarIcon } from '@components/Icons/StarIcon/StarIcon';
import { ForkIcon } from '@components/Icons/ForkIcon/ForkIcon';
import { Modal } from '@components/Modal/Modal';
import { RepoPreview } from '@features/repo/components/Repo/RepoPreview';
import './Repo.css';

type TProps = {
  repo: IRepo;
  handlePrev: () => void;
  handleNext: () => void;
};

export const Repo: FC<TProps> = ({ repo, handlePrev, handleNext }) => {
  const [showPreview, setShowPreview] = useState(false);

  return (
    <>
      <div className={classNames('repo')}>
        <ArrowIcon className="repo__arrow-button" position="left" onClick={handlePrev} />
        <div
          className="info__container"
          onClick={(event) => {
            event.stopPropagation();
            setShowPreview(!showPreview);
          }}
        >
          <div className="repo__info">
            <p className="repo__name">{repo.name}</p>
            <p className="repo__description">{repo.description}</p>
            <div className="repo__counts">
              <div className="counts__item">
                <StarIcon />
                <p className="counts__title">{repo.stargazers_count}</p>
              </div>
              <div className="counts__item">
                <ForkIcon />
                <p className="counts__title">{repo.forks_count}</p>
              </div>
            </div>
          </div>
        </div>
        <ArrowIcon className="repo__arrow-button" position="right" onClick={handleNext} />
      </div>

      <Modal setShow={setShowPreview} show={showPreview}>
        <RepoPreview repo={repo} />
      </Modal>
    </>
  );
};
