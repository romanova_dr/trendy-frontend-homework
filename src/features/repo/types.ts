export interface IRepo {
  id: number;
  name: string;
  homepage: string;
  html_url: string;
  description: string;
  forks_count: number;
  stargazers_count: number;
  topics: string[];
  owner: {
    login: string;
  };
}
