import { RootState } from '@app/store';
import { IRepo } from './types';

export const getRepos = (state: RootState): IRepo[] => state.reposList.repos;
