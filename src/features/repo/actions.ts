import { createAsyncThunk } from '@reduxjs/toolkit';
import { fetchRepos } from '@app/api';
import { setRepos } from './slice';
import { IRepo } from '@features/repo/types';

export const fetchNews = createAsyncThunk('api/fetchRepos', (_, { dispatch }) => {
  return fetchRepos().then((repos) => {
    dispatch(setRepos(repos));
    return repos as IRepo[];
  });
});
