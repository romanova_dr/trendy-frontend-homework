import { applyScheme, getSystemScheme } from './colorSchemeUtils';

applyScheme(getSystemScheme());
