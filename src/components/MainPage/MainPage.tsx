import React, { FC } from 'react';
import { Content } from '@components/Content/Content';
import { Header } from '@components/Header/Header';
import './MainPage.css';

export const MainPage: FC = () => {
  return (
    <div className="main-page">
      <Header />
      <Content />
    </div>
  );
};
