import React, { FC, HTMLAttributes, useEffect } from 'react';
import classNames from 'classnames';
import { createPortal } from 'react-dom';
import { CSSTransition } from 'react-transition-group';
import { CrossIcon } from '@components/Icons/CrossIcon/CrossIcon';
import './Modal.css';

interface ModalProps extends HTMLAttributes<HTMLElement> {
  show: boolean;
  setShow: (value: boolean) => void;
}

export const Modal: FC<ModalProps> = ({ show, setShow, className, children, ...props }: ModalProps) => {
  useEffect(() => {
    if (show) {
      document.documentElement.classList.add('prevent-scroll');
    }
    return () => {
      document.documentElement.classList.remove('prevent-scroll');
    };
  }, [show]);

  useEffect(() => {
    const documentKeydownListener = (event: KeyboardEvent) => {
      if (event.key === 'Escape') {
        setShow(false);
      }
    };

    if (show) {
      document.addEventListener('keydown', documentKeydownListener);
    } else {
      document.removeEventListener('keydown', documentKeydownListener);
    }
    return () => {
      document.removeEventListener('keydown', documentKeydownListener);
    };
  }, [show, setShow]);

  useEffect(() => {
    const documentClickListener = () => {
      setShow(false);
    };

    if (show) {
      document.addEventListener('click', documentClickListener);
    }
    return () => {
      document.removeEventListener('click', documentClickListener);
    };
  }, [show, setShow]);

  return createPortal(
    <CSSTransition in={show} mountOnEnter={true} unmountOnExit={true} timeout={400} classNames="modal-animation">
      <div className={classNames('modal', className)} onClick={() => setShow(false)} {...props}>
        <div className="modal__children" onKeyDown={(e) => e.stopPropagation()} onClick={(e) => e.stopPropagation()}>
          <CrossIcon className="modal__exit" onClick={() => setShow(false)} />
          {children}
        </div>
      </div>
    </CSSTransition>,
    document.getElementById('overlay') as HTMLElement
  );
};
