import React, { FC } from 'react';
import classNames from 'classnames';
import { repeat } from '@app/utils';
import './SkeletonText.css';

interface SkeletonTextProps {
  rowsCount?: number;
  primary?: boolean;
}

export const SkeletonText: FC<SkeletonTextProps> = ({ primary = false, rowsCount = 1 }: SkeletonTextProps) => {
  return (
    <span
      className={classNames('skeleton-text', {
        'skeleton-text_primary': primary,
      })}
    >
      {repeat((i) => {
        return <span key={i} className="skeleton-text__row skeleton-gradient" />;
      }, rowsCount)}
    </span>
  );
};
