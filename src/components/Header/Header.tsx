import React, { FC } from 'react';
import classNames from 'classnames';
import { ThemeIcon } from '@components/Icons/Theme/ThemeIcon';
import './Header.css';
import { useColorScheme } from '@features/colorScheme/hooks';

export const Header: FC = () => {
  const { userScheme, setUserScheme } = useColorScheme();

  const onHandleTheme = () => {
    setUserScheme(userScheme === 'dark' ? 'light' : 'dark');
  };
  return (
    <div className={classNames('header')}>
      <p className={classNames('header__title')}>Топ популярных javascript репозиториев</p>
      <ThemeIcon className={classNames('header__toggler')} onClick={onHandleTheme} />
    </div>
  );
};
