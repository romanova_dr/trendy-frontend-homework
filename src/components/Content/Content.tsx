import React, { FC, useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import Fade from 'react-reveal/Fade';
import { Dispatch } from '@app/store';
import { IRepo } from '@features/repo/types';
import { fetchNews } from '@features/repo/actions';
import { getRepos } from '@features/repo/selectors';
import { Repo } from '@features/repo/components/Repo/Repo';
import { RepoSkeleton } from '@features/repo/components/Repo/RepoSkeleton';
import './Content.css';

export const Content: FC = () => {
  const dispatch = useDispatch<Dispatch>();
  const repos = useSelector(getRepos);

  const [slideType, setSlideType] = useState<'prev' | 'next'>('next');
  const [swipe, setSwipe] = useState(true);
  const [loading, setLoading] = useState(false);
  const [currentRepo, setCurrentRepo] = useState<IRepo | null>(null);
  const [currentIndex, setCurrentIndex] = useState(0);

  useEffect(() => {
    setLoading(true);
    dispatch(fetchNews()).finally(() => {
      setLoading(false);
    });
  }, []);

  useEffect(() => {
    setCurrentRepo(repos[0]);
  }, [repos]);

  useEffect(() => {
    setTimeout(() => {
      setSwipe(true);
    }, 500);
  }, [currentRepo]);

  const handleNext = () => {
    setSwipe(false);
    setSlideType('prev');
    if (currentIndex === repos.length - 1) {
      setCurrentIndex(0);
      setCurrentRepo(repos[0] ?? null);
    } else {
      setCurrentIndex(currentIndex + 1);
      setCurrentRepo(repos[currentIndex + 1] ?? null);
    }
  };

  const handlePrev = () => {
    setSwipe(false);
    setSlideType('next');
    if (currentIndex === 0) {
      setCurrentIndex(repos.length - 1);
      setCurrentRepo(repos[repos.length - 1] ?? null);
    } else {
      setCurrentIndex(currentIndex - 1);
      setCurrentRepo(repos[currentIndex - 1] ?? null);
    }
  };

  if (loading) {
    return (
      <div className="content">
        <RepoSkeleton />
      </div>
    );
  }

  if (!currentRepo) {
    return (
      <div className="content">
        <p className="content__error">Не удалось загрузить ни одного репозитория 😥</p>
      </div>
    );
  }

  return (
    <div className="content">
      <Fade left={slideType === 'next'} right={slideType === 'prev'} opposite when={swipe} duration={500}>
        <Repo key={currentRepo?.id} repo={currentRepo} handleNext={handleNext} handlePrev={handlePrev} />
      </Fade>
    </div>
  );
};
